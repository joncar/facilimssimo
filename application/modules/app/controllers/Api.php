<?php

require_once APPPATH . '/controllers/Panel.php';

class Api extends Main {

    function __construct() {
        parent::__construct();
        $this->load->library('grocery_crud');
        $this->load->library('ajax_grocery_crud');
        $this->load->model('elements_app');
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: *");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            }

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }

            exit(0);
        }

        $postdata = file_get_contents("php://input");
        if (isset($postdata)) {
            $_POST = (array) json_decode($postdata);
        } else {
            $_POST = array();
        }

        date_default_timezone_set('Europe/Madrid');
        setlocale(LC_ALL, 'Spanish');
    }

    function printjson($object) {
        header('Content-Type: application/json');
        echo json_encode($object);
    }

    /*     * **** LOGIN **** */

    function registro($act = 0) {
        $response = array('success' => false, 'msj' => 'Complete los datos requeridos');
        $this->form_validation->set_rules('password', 'Password', 'required')
                ->set_rules('terminos', 'terminos', 'required');
        if ($this->form_validation->run()) {

            if (!$act) {
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.email]');
                if ($this->form_validation->run()) {
                    $_POST['password'] = md5($_POST['password']);
                    $this->db->insert('user', $_POST);
                    $response['primary_key'] = $this->db->insert_id();
                    $response['success'] = true;
                    $response['msj'] = 'Usuario añadido con éxito';
                } else {
                    $response['msj'] = !empty($_POST) ? $this->form_validation->error_string() : $response['msj'];
                }
            } else {
                $user = $this->elements_app->user(array('email' => $_POST['email']));
                if ($user->num_rows() > 0) {
                    $user = $user->row();
                    if ($user->password != $_POST['password']) {
                        $_POST['password'] = md5($_POST['password']);
                    }
                    $this->db->update('user', $_POST, array('id' => $_POST['id']));
                    $response['success'] = true;
                    $response['msj'] = 'Usuario actualizado con éxito';
                } else {
                    $response['msj'] = 'Ocurrió un error al actualizar';
                }
            }
        } else {
            $response['msj'] = !empty($_POST) ? $this->form_validation->error_string() : $response['msj'];
        }

        $this->printjson($response);
    }

    function login() {
        $response = array('success' => false, 'msj' => 'Complete los datos requeridos');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email')
                ->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run()) {
            $user = $this->elements_app->user(array('email' => $_POST['email'], 'password' => md5($_POST['password'])));
            if ($user->num_rows() == 0) {
                $response['msj'] = 'Usuario o contraseña incorrecta';
            } elseif (!$user->row()->status) {
                $response['msj'] = 'Su usuario se encuentra bloqueado, por favor comunícate con el administrador del sistema para más información';
            } else {
                $response = array(
                    'success' => true,
                    'msj' => $user->row()
                );
            }
        } else {
            $response['msj'] = !empty($_POST) ? $this->form_validation->error_string() : $response['msj'];
        }

        $this->printjson($response);
    }

    function recover() {
        $response = array('success' => false, 'msj' => 'Completa los datos para recuperar');
        if (!empty($_POST)) {
            if (!empty($_POST['email'])) {
                $usuario = $this->db->get_where('user', array('email' => $_POST['email']));
                if ($usuario->num_rows() == 0) {
                    $response['msj'] = 'El email ingresado no existe';
                } else {
                    $response['success'] = true;
                    $response['msj'] = 'Los pasos para restauración han sido enviados a su Email';
                    $post = $usuario->row();
                    $post->link = base_url('registro/recuperar_app/' . base64_encode($post->email));
                    //echo $post->link;
                    //$this->enviarcorreo($post,15);
                }
            } else {
                $response['msj'] = 'Debe indicar los datos para realizar su conexión';
            }
        } else {
            $response['msj'] = 'Debe indicar los datos para realizar su conexión';
        }

        echo json_encode($response);
    }

    /*     * ********* END LOGIN *********** */

    /*     * ********** Actualizar Patrón ************** */

    function actualizarPatron() {
        $response = array('success' => false, 'msj' => 'Completa los datos para actualizar');
        $this->form_validation->set_rules('nombre', 'Nombre completo del patrón', 'required')
                ->set_rules('id', 'Usuario', 'required|numeric')
                ->set_rules('razon_social', 'Razón social', 'required');
        if ($this->form_validation->run()) {
            $nombre = $_POST['nombre'];
            $razonSocial = $_POST['razon_social'];
            $domicilio = empty($_POST['domicilio']) ? '' : $_POST['domicilio'];
            $cp = empty($_POST['cp']) ? '' : $_POST['cp'];
            $direccion = empty($_POST['direccion']) ? '' : $_POST['direccion'];
            $rfc = empty($_POST['rfc']) ? '' : $_POST['rfc'];
            $factura = empty($_POST['factura']) ? '0' : $_POST['factura'];
            $this->db->update('user', array(
                'nombre' => $nombre,
                'razon_social' => $razonSocial,
                'domicilio' => $domicilio,
                'cp' => $cp,
                'direccion' => $direccion,
                'rfc' => $rfc,
                'factura' => $factura,
                    ), array('id' => $_POST['id']));
            $response['success'] = true;
            $response['msj'] = 'Alta exitosa';
        } else {
            $response['msj'] = !empty($_POST) ? $this->form_validation->error_string() : $response['msj'];
        }


        $this->printjson($response);
    }

    /*     * ********** END Actualizar Patrón ************** */

    /*     * ********* ALTA *************** */

    function planes() {
        $response = array();
        if (!empty($_POST['datos'])) {
            $planes = $this->elements_app->planes();
            foreach ($planes->result() as $p) {
                $p->precio = $this->elements_app->getTarifa($p, $_POST['datos']);
                $response[] = $p;
            }
        }
        $this->printjson($response);
    }
    
    function ver_plan($id) {
        $response = array();
        $planes = $this->elements_app->planes(array('planes.id'=>$id));
        foreach ($planes->result() as $p) {            
            $response[] = $p;
        }
        $this->printjson($response);
    }

    function contratar() {
        $_POST['tarjeta_numero'] = isset($_POST['tarjeta_numero']) ? str_replace(' ', '', $_POST['tarjeta_numero']) : $_POST['tarjeta_numero'];
        $response = array('success' => false, 'msj' => 'Completa los datos para recuperar');
        $this->form_validation->set_rules('patronos_id', 'Patrono', 'required')
                ->set_rules('fecha_nacimiento', 'Fecha de nacimiento', 'required')
                ->set_rules('plan_id', 'Plan de pago', 'required')
                ->set_rules('intervalo', 'Periodo de pago', 'required')
                ->set_rules('importe', 'Importe de pago', 'required')
                ->set_rules('nombre', 'Nombre del empleado', 'required')
                ->set_rules('email', 'Correo electrónico del empleado', 'required|valid_email')
                ->set_rules('nro_seg_social', 'Numero de seguridad social', 'required')
                ->set_rules('domicilio', 'Domicilio', 'required')
                ->set_rules('codigo_postal', 'Código Postal', 'required')
                ->set_rules('curp', 'CURP', 'required')
                ->set_rules('salario_base_diario', 'Salario base', 'required|numeric')
                ->set_rules('cant_dias_trabajados', 'Cantidad de dias trabajados', 'required|numeric')                
                ->set_rules('sexo', 'Sexo', 'required')
                ->set_rules('estado_nacimiento', 'Estado de nacimiento', 'required')
                ->set_rules('apellido', 'Apellido Paterno', 'required')
                ->set_rules('tipoPago', 'Forma de pago', 'required|numeric');
        if(!empty($_POST['tipoPago']) && $_POST['tipoPago']==1){
                $this->form_validation->set_rules('tarjeta_nombre', 'Nombre en la tarjeta', 'required')
                                                   ->set_rules('tarjeta_numero', 'Numéro de tarjeta', 'required|numeric')
                                                   ->set_rules('tarjeta_vencimiento', 'Vencimiento de tarjeta', 'required')
                                                   ->set_rules('tarjetacvc', 'CVC', 'required|numeric');   
        }
        if ($this->form_validation->run()) {
            $_POST['fecha_nacimiento'] = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['fecha_nacimiento'])));
            $_POST['edad'] = getAge($_POST['fecha_nacimiento']);
            $this->db->insert('empleados', array(
                'patronos_id' => $_POST['patronos_id'],
                'nombre' => $_POST['nombre'],
                'nro_seg_social' => $_POST['nro_seg_social'],
                'domicilio' => $_POST['domicilio'],
                'codigo_postal' => $_POST['codigo_postal'],
                'curp' => $_POST['curp'],
                'salario_base_diario' => $_POST['salario_base_diario'],
                'cant_dias_trabajados' => $_POST['cant_dias_trabajados'],
                'edad' => $_POST['edad'],
                'fecha_nacimiento' => $_POST['fecha_nacimiento'],
                'intervalo' => $_POST['intervalo'], 
                'planes_id' => $_POST['plan_id'],
                'importe' => $_POST['importe'],
                'estado_nacimiento' => $_POST['estado_nacimiento'],
                'sexo' => $_POST['sexo'],
                'apellido' => $_POST['apellido'],
                'apellido2' => $_POST['apellido2'],
                'email'=>$_POST['email']
            ));
            $id = $this->db->insert_id();
            $_POST['empleados_id'] = $id;
            /* if(!empty($_POST['planes'])){
              foreach($_POST['planes'] as $n=>$v){
              if($v){
              $this->db->insert('empleados_planes',array(
              'planes_id'=>$n,
              'empleados_id'=>$id,
              'precio_plan'=>$this->db->get_where('planes',array('id'=>$n))->row()->precio
              ));
              }
              }
              } */
            //INtento de realizar pago 
            if ($this->pay()) {
                $response['success'] = true;
                if($_POST['tipoPago']==1){
                    $response['msj'] = 'Alta exitosa';
                }else{
                    $this->db->order_by('id','DESC');
                    $this->db->where('empleados_id',$_POST['empleados_id']);
                    $ref = $this->db->get_where('transacciones')->row()->referencia;
                    $response['msj'] = 'Alta exitosa';
                    $response['link'] = 'https://sandbox-dashboard.openpay.mx/paynet-pdf/mltapjuipjzmv3fjlz4y/'.$ref;
                }
            } else {
                $response['success'] = true;
                $response['msj'] = 'Se ha registrado el empleado, pero su pago no ha sido aprobado';
            }
        } else {
            $response['msj'] = !empty($_POST) ? $this->form_validation->error_string() : $response['msj'];
        }


        $this->printjson($response);
    }

    /*     * ******* END ALTA *********** */
    /*     * ********* BAJA ************ */

    function empleados($act = 'list') {
        if ($act == 'list') {
            if (!empty($_POST['patronos_id'])) {
                $response = array();
                foreach ($this->elements_app->empleados(array('patronos_id' => $_POST['patronos_id'], 'status' => 1))->result() as
                        $e) {
                    $response[] = $e;
                }
                $this->printjson($response);
            }
        }
        if ($act == 'update') {
            $response = array('success' => false, 'msj' => 'Por favor completa los datos requeridos');
            $this->form_validation->set_rules('empleados_id', 'Empleado', 'required|numeric')
                    ->set_rules('asistencia', 'Asistencia', 'required|numeric')
                    ->set_rules('limpieza', 'Limpieza', 'required|numeric')
                    ->set_rules('puntualidad', 'Puntualidad', 'required|numeric')
                    ->set_rules('desempeno', 'Desempeno', 'required|numeric');
            if ($this->form_validation->run()) {
                $_POST['status'] = 0;
                $data = $_POST;
                unset($data['empleados_id']);
                $this->db->update('empleados', $data, array('id' => $_POST['empleados_id']));
                $response['msj'] = 'Baja exitosa';
                $response['success'] = true;
            } else {
                $response['msj'] = $this->form_validation->error_string();
            }
            echo $this->printjson($response);
        }
    }

    function transacciones($act = 'list') {
        if ($act == 'list') {
            if (!empty($_POST['patronos_id'])) {
                $response = array();
                $total = 0;
                $this->db->order_by('id', 'DESC');
                foreach ($this->elements_app->transacciones(array('transacciones.patronos_id' => $_POST['patronos_id']))->result() as
                        $e) {
                    $total+= $e->importe;
                    $response[] = $e;
                }
                $total = number_format($total,0,'.',',');
                $response = array('total' => $total, 'data' => $response);
                $this->printjson($response);
            }
        }
    }

    function parametros($act = 'list') {
        $response = array();
        foreach ($this->elements_app->parametros()->result() as
                $e) {
            $response[] = $e;
        }
        $this->printjson($response);
    }

    /*     * ********* END BAJA ************ */

    function pay() {

        if (!empty($_POST['empleados_id']) && !empty($_POST['tipoPago'])) {

            require_once APPPATH . 'libraries/openpay/Openpay.php';
            require_once APPPATH . 'libraries/openpay/resources/OpenpayCustomer.php';
            $_POST['tarjeta_numero'] = str_replace(' ', '', $_POST['tarjeta_numero']);
            $email = $this->db->get_where('user', array('id' => $_POST['patronos_id']))->row()->email;
            $empleado = $this->db->get_where('empleados', array('id' => $_POST['empleados_id']))->row();
            $total = $empleado->importe;            
            $openpay = Openpay::getInstance('mltapjuipjzmv3fjlz4y', 'sk_c8a7ff73918646ee9d16cbbfdb8eb979');
            $this->db->insert('transacciones', array(
                'patronos_id' => $_POST['patronos_id'],
                'empleados_id' => $_POST['empleados_id'],
                'fecha_transaccion' => date("Y-m-d H:i:s"),
                'importe' => $total,
                'dias_trabajados' => $_POST['cant_dias_trabajados'],
                'periodo' => date("Y-m-d"),
                'status' => 0,
                'tipo'=>$_POST['tipoPago']
            ));
            $id = $this->db->insert_id();
            
            
                try {                    
                    $customerData = array(
                        'name' => $_POST['nombre'],
                        'email' => $email,
                        'requires_account' => false
                    );
                    $openpay->customers->add($customerData);
                    $customers = $openpay->customers->getList(array('offset' => 0, 'limit' => 5));
                    $customer = $openpay->customers->get($customers[0]->id);
                    
                    if($_POST['tipoPago']==1){
                        $mes = date("m", strtotime($_POST['tarjeta_vencimiento']));
                        $anio = date("y", strtotime($_POST['tarjeta_vencimiento']));
                        $cardData = array(
                            'holder_name' => $_POST['tarjeta_nombre'],
                            'card_number' => $_POST['tarjeta_numero'],
                            'cvv2' => $_POST['tarjetacvc'],
                            'expiration_month' => $mes,
                            'expiration_year' => $anio
                        );
                        $card = $customer->cards->add($cardData);
                        $card = $customer->cards->getList(array('offset' => 0));
                        $card = $card[0];
                        $chargeData = array(
                            'source_id' => $card->id,
                            'method' => 'card',
                            'amount' => $total,
                            'description' => 'Registro de nuevo empleado ' . $_POST['nombre'],
                            'order_id' => 'ORDEN-' . date("is") . $_POST['empleados_id'],
                            'device_session_id' => date("Ymdhis"));
                        $charge = $customer->charges->create($chargeData);
                        $this->db->update('transacciones', array('status' => 1, 'resultado' => 'Pago satisfactorio'), array('id' => $id));
                        $this->db->update('empleados', array('nro_tarjeta' => $card->id, 'conektaUser' => $customers[0]->id), array('id' => $_POST['empleados_id']));                        
                    }else{
                        $chargeData = array(                            
                            'method' => 'store',
                            'amount' => $total,
                            'description' => 'Registro de nuevo empleado ' . $_POST['nombre'],
                            'order_id' => 'ORDEN-' . date("is") . $_POST['empleados_id'],
                            'device_session_id' => date("Ymdhis"));
                        $charge = $customer->charges->create($chargeData);                                               
                        $this->db->update('transacciones', array('referencia' => $charge->payment_method->reference, 'resultado' => 'En espara de cobro'), array('id' => $id));
                        $this->db->update('empleados', array('conektaUser' => $customers[0]->id), array('id' => $_POST['empleados_id']));                        
                    }
                    return true;
                    
                } catch (OpenpayApiTransactionError $e) {
                    $this->db->update('transacciones', array('resultado' => $e->getMessage()), array('id' => $id));
                    if (!empty($card)) {
                        $this->db->update('empleados', array('nro_tarjeta' => $card->id, 'conektaUser' => $customers[0]->id), array('id' => $_POST['empleados_id']));
                    }                     
                    return false;
                } catch (OpenpayApiRequestError $e) {
                    $this->db->update('transacciones', array('resultado' => $e->getMessage()), array('id' => $id));
                    if (!empty($card)) {
                        $this->db->update('empleados', array('nro_tarjeta' => $card->id, 'conektaUser' => $customers[0]->id), array('id' => $_POST['empleados_id']));
                    }                     
                    return false;
                }
            
            }
    }

    function updateCard() {
        $_POST['tarjeta_numero'] = isset($_POST['tarjeta_numero']) ? str_replace(' ', '', $_POST['tarjeta_numero']) : $_POST['tarjeta_numero'];
        $response = array('success' => false, 'msj' => 'Ocurrio un error al actualizar su tarjeta');
        $this->form_validation->set_rules('transaccion', '#Transacción', 'required')
                ->set_rules('tarjeta_nombre', 'Nombre en la tarjeta', 'required')
                ->set_rules('tarjeta_numero', 'Numéro de tarjeta', 'required|numeric')
                ->set_rules('tarjeta_vencimiento', 'Vencimiento de tarjeta', 'required')
                ->set_rules('tarjetacvc', 'CVC', 'required|numeric');
        if ($this->form_validation->run()) {
            $transaccion = $this->db->get_where('transacciones', array('id' => $_POST['transaccion']));
            if ($transaccion->num_rows() > 0) {
                $transaccion = $transaccion->row();
                $_POST['tarjeta_numero'] = str_replace(' ', '', $_POST['tarjeta_numero']);
                $email = $this->db->get_where('user', array('id' => $transaccion->patronos_id))->row()->email;
                $empleado = $this->db->get_where('empleados', array('id' => $transaccion->empleados_id))->row();
                $mes = date("m", strtotime($_POST['tarjeta_vencimiento']));
                $anio = date("y", strtotime($_POST['tarjeta_vencimiento']));
                require_once APPPATH . 'libraries/openpay/Openpay.php';
                require_once APPPATH . 'libraries/openpay/resources/OpenpayCustomer.php';
                $openpay = Openpay::getInstance('mltapjuipjzmv3fjlz4y', 'sk_c8a7ff73918646ee9d16cbbfdb8eb979');

                try {
                    $customerData = array(
                        'name' => $_POST['tarjeta_nombre'],
                        'email' => $email,
                        'requires_account' => false
                    );
                    $openpay->customers->add($customerData);
                    $customers = $openpay->customers->getList(array('offset' => 0, 'limit' => 5));
                    $customer = $openpay->customers->get($customers[0]->id);
                    $cardData = array(
                        'holder_name' => $_POST['tarjeta_nombre'],
                        'card_number' => $_POST['tarjeta_numero'],
                        'cvv2' => $_POST['tarjetacvc'],
                        'expiration_month' => $mes,
                        'expiration_year' => $anio
                    );
                    $card = $customer->cards->add($cardData);
                    $card = $customer->cards->getList(array('offset' => 0));
                    $card = $card[0];
                    $this->db->update('empleados', array('nro_tarjeta' => $card->id, 'conektaUser' => $customers[0]->id), array('id' => $empleado->id));
                    $response = array(
                        'success' => true,
                        'msj' => 'Información de pago actualizada con éxito'
                    );
                    $this->printjson($response);
                } catch (OpenpayApiTransactionError $e) {
                    $this->printjson($response);
                    if (!empty($card)) {
                        $this->db->update('empleados', array('nro_tarjeta' => $card->id, 'conektaUser' => $customers[0]->id), array('id' => $empleado->id));
                    }
                } catch (OpenpayApiRequestError $e) {
                    $this->printjson($response);
                    if (!empty($card)) {
                        $this->db->update('empleados', array('nro_tarjeta' => $card->id, 'conektaUser' => $customers[0]->id), array('id' => $empleado->id));
                    }
                }
            }
        } else {
            $response = array('success' => false, 'msj' => $this->form_validation->error_string());
            $this->printjson($response);
        }
    }

    function retryPay() {

        if (!empty($_POST['transacciones_id'])) {

            require_once APPPATH . 'libraries/openpay/Openpay.php';
            require_once APPPATH . 'libraries/openpay/resources/OpenpayCustomer.php';
            $transaccion = $this->db->get_where('transacciones', array('id' => $_POST['transacciones_id']));
            $response = array('success' => false, 'msj' => 'Transacción no encontrada o hubo un problema al procesar su pago');
            if ($transaccion->num_rows() > 0) {
                $transaccion = $transaccion->row();
                $id = $transaccion->id;
                $email = $this->db->get_where('user', array('id' => $transaccion->patronos_id))->row()->email;
                $empleado = $this->db->get_where('empleados', array('id' => $transaccion->empleados_id))->row();
                $total = $empleado->importe;

                if (empty($empleado->conektaUser) || empty($empleado->nro_tarjeta)) {
                    $response = array(
                        'success' => false,
                        'msj' => 'Se debe actualizar la información de pago'
                    );
                    $this->printjson($response);
                    return false;
                }

                $openpay = Openpay::getInstance('mltapjuipjzmv3fjlz4y', 'sk_c8a7ff73918646ee9d16cbbfdb8eb979');
                try {
                    $customer = $openpay->customers->get($empleado->conektaUser);
                    $chargeData = array(
                        'source_id' => $empleado->nro_tarjeta,
                        'method' => 'card',
                        'amount' => $total,
                        'description' => 'Registro de nuevo empleado ' . $empleado->nombre,
                        'order_id' => 'ORDEN-' . date('is') . $transaccion->empleados_id,
                        'device_session_id' => date("Ymdhis"));
                    $charge = $customer->charges->create($chargeData);
                    $this->db->update('transacciones', array('status' => 1, 'resultado' => 'Pago satisfactorio'), array('id' => $id));
                    $response = array(
                        'success' => true,
                        'Pago completado con éxito'
                    );
                    $this->printjson($response);
                } catch (OpenpayApiTransactionError $e) {
                    $this->db->update('transacciones', array('resultado' => $e->getMessage()), array('id' => $id));
                    $this->printjson($response);
                } catch (OpenpayApiRequestError $e) {
                    $this->db->update('transacciones', array('resultado' => $e->getMessage()), array('id' => $id));
                    $this->printjson($response);
                }
            }
        }
    }

    function getCURP() {
        $response = array('success' => false, 'msj' => 'Complete los datos solicitados');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required')
                ->set_rules('sexo', 'Sexo', 'required')
                ->set_rules('fecha_nacimiento', 'Fecha de nacimiento', 'required')
                ->set_rules('apellido', 'Apellido Paterno', 'required')
                ->set_rules('estado_nacimiento', 'Estado de nacimiento', 'required');
        if ($this->form_validation->run()) {
            $response = array('success' => true, 'msj' => $this->elements_app->generateCURP($_POST));
        } else {
            $response = array('success' => false, 'msj' => $this->form_validation->error_string());
        }

        $this->printjson($response);
    }

    function test() {
        if(empty($_GET)){
            $img = 'https://serviciosdigitales.imss.gob.mx/gestionAsegurados-web-externo/servlet/CaptchaServlet';
            $image = base64_encode(file_get_contents($img));
            $image = 'data: '.mime_content_type($img).';base64,'.$image;
            echo '<form method="get"><img src="'.$image.'"> <input type="text" name="captcha"><button type="submit">Enviar</button></form>';
        }else{
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://serviciosdigitales.imss.gob.mx/gestionAsegurados-web-externo/asignacionNSS/valida");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,"captcha=".$_GET['captcha']."&curp=SUFJ820224HDFLNN07&correoElectronico.correo=jsulkin@gmail.com&correoElectronicoFiscal.correo=jsulkin@gmail.com");            
            $userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.56 Safari/536.5";            
            $cookieJar = dirname(__FILE__) . '/cookiez_.tmp';
            curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);        
            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieJar);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieJar);
            $server_output = curl_exec($ch);
            curl_close ($ch);
            echo $server_output;
        }
    }

}
