<?php 
require_once APPPATH.'controllers/Panel.php'; 
class Administracion extends Panel{
	function __construct(){
		parent::__construct();
	}

	function patronos(){
		$crud=$this->crud_function("","");
		$output=$crud->render();
		$this->loadView($output);

	}
	function empleados(){
		$crud=$this->crud_function("","");
		$output=$crud->render();
		$this->loadView($output);
	}
	function planes(){
		$crud=$this->crud_function("","");
		if($crud->getParameters()!='list'){
			$crud->display_as('formula','Formula <a href="#help" data-toggle="modal"><i class="fa fa-question "></i></a>');
			$crud->display_as('intervalo','
				Intervalo (Cada cuantos días se harán los cobros)
			');
		}
		$output=$crud->render();
		$output->output = $output->output.$this->load->view('_ayudaModal',array(),TRUE); 
		$this->loadView($output);
	}
	function transacciones(){
		$crud=$this->crud_function("","");
		$output=$crud->render();
		$this->loadView($output);
	}
	function formulas($x = ''){
		if($x=='simulador'){
			$this->loadView(array('view'=>'panel','output'=>$this->load->view('simulador',array(),TRUE),'crud'=>'user'));
			return false;
		}
		$crud=$this->crud_function("","");
		$crud->columns('id','nombre','formula');
		$crud->field_type('formula','editor',array('type'=>'textarea'));
		$crud->display_as('formula','Formula <a href="#help" data-toggle="modal"><i class="fa fa-question "></i></a>');
		$crud->set_clone();
		$output=$crud->render();
		$output->output = '<a href="'.base_url('administracion/formulas/simulador').'" class="btn btn-info">Simulador</a>'.$output->output.$this->load->view('_ayudaModal',array(),TRUE);
		$this->loadView($output);
	}
	function tarifas_edades(){
		$crud=$this->crud_function("","");
		$crud->set_subject('Tarifa');
		$output=$crud->render();
		$output->title = 'Tarifa de edades';
		$this->loadView($output);
	}
}
?>