<div id="help" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Variables para composición de formulas</h4>
      </div>
      <table class="table">
      	<thead>
      		<tr>
      			<th>Variable</th>
      			<th>Definición</th>
      		</tr>
      	</thead>
      	<tbody>
      		<tr>
      			<td>TE</td>
      			<td>Tarifa por edad</td>
      		</tr>
      		<tr>
      			<td>SA</td>
      			<td>Salario base diario</td>
      		</tr>
          <tr>
            <td>DL</td>
            <td>Días laborables</td>
          </tr>
          <tr>
            <td>DM</td>
            <td>Cuantos días contiene mes</td>
          </tr>
      		<?php 
      			$this->load->model('elements_app');
      			foreach($this->elements_app->formulas()->result() as $f): ?>
      		<tr>
      			<td>F<?= $f->id ?></td>
      			<td><?= $f->nombre ?></td>
      		</tr>
      		<?php endforeach ?>
      	</tbody>
      </table>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->