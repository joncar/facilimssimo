<div class="panel panel-default">
	<div class="panel-heading">
		<div class="panel-title">Simulador de pago <a href="#help" data-toggle="modal"><i class="fa fa-question"></i></a></div>
	</div>
	<div class="panel-body">
		<form action="" method="post">
			<div class="form-group">
				<label for="">Formula</label>
				<input type="text" name="formula" class="form-control" value="<?= @$_POST['formula'] ?>">
			</div>
			<div class="form-group">
				<label for="">Salario Base / dia</label>
				<input type="number" name="salario_base_diario" class="form-control" value="<?= @$_POST['salario_base_diario'] ?>">
			</div>
			<div class="form-group">
				<label for="">Edad</label>
				<input type="number" name="edad" class="form-control" value="<?= @$_POST['edad'] ?>">
			</div>
			<div class="form-group">
				<label for="">Dias laborables</label>
				<input type="number" name="cant_dias_trabajados" class="form-control" value="<?= @$_POST['cant_dias_trabajados'] ?>">
			</div>
			<div class="form-group">
				<label for="">Mes</label>
				<input type="number" name="mes" class="form-control" value="<?= empty($_POST['mes'])?date("m"):$_POST['mes'] ?>">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-info">Calcular</button>
			</div>
		</form>
	</div>
</div>

<?php if(!empty($_POST)): $this->load->model('elements_app') ?>
	<div  style="min-height: 100vh;">
		<h1>Resultado</h1>
		<div class="row">
			<?php if(empty($_POST['formula'])): ?>
				<?php foreach($this->elements_app->planes()->result() as $p): ?>	
					<div class="col-xs-12 col-md-4">
						<h3><?= $p->nombre ?>:</h3>
						<p>Formula: <?= $p->formula ?></p>
						<p>Resultado: <?= $this->elements_app->getTarifa($p,(object)$_POST,FALSE); ?></p>
					</div>
				<?php endforeach ?>
			<?php else: ?>
				<h3>Formula personalizada:</h3>
				<p>Formula: <?= $_POST['formula'] ?></p>
				<p>Resultado: <?= $this->elements_app->getTarifaWithFormula($_POST['formula'],(object)$_POST,FALSE); ?></p>
			<?php endif ?>
		</div>
	</div>
<?php endif ?>
<?php $this->load->view('_ayudaModal',array(),FALSE); ?>