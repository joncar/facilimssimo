<?php

class Elements_app extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function user($where = array()){
		$data = $this->db->get_where('user',$where);
		foreach($data->result() as $n=>$v){
			//$data->row($n)->foto = base_url();
		}
		return $data;
	}

	function planes($where = array()){
		$data = $this->db->get_where('planes',$where);
		foreach($data->result() as $n=>$v){
			//$data->row($n)->foto = base_url();
		}
		return $data;
	}



	function empleados($where = array()){
		$data = $this->db->get_where('empleados',$where);
		foreach($data->result() as $n=>$v){
			//$data->row($n)->foto = base_url();
		}
		return $data;
	}

	function transacciones($where = array()){
		$this->db->select('transacciones.*,empleados.nombre, empleados.apellido, empleados.planes_id,planes.intervalo');
		$this->db->join('empleados','empleados.id = transacciones.empleados_id');
                                   $this->db->join('planes','planes.id = empleados.planes_id');
		$data = $this->db->get_where('transacciones',$where);		
		foreach($data->result() as $n=>$v){
                                        $data->row($n)->_fecha_transaccion = $v->fecha_transaccion;
                                        $data->row($n)->fecha_transaccion = date("d/m/Y",strtotime($v->fecha_transaccion));
                                        $data->row($n)->desde  = date("Y-m-d",strtotime($v->periodo.' -'.$v->intervalo.' days'));
                                        $data->row($n)->periodoFormat = date("d/m/Y",strtotime($data->row($n)->desde)).' - '.date("d/m/Y",strtotime($v->periodo));
                                        
		}		
		return $data;
	}

	function parametros($where = array()){
		$data = $this->db->get_where('parametros',$where);
		foreach($data->result() as $n=>$v){
			//$data->row($n)->foto = base_url();
		}
		return $data;
	}

	function formulas($where = array()){
		$data = $this->db->get_where('formulas',$where);
		foreach($data->result() as $n=>$v){
			//$data->row($n)->foto = base_url();
		}
		return $data;
	}

	function tarifas_edades($where = array()){
		$data = $this->db->get_where('tarifas_edades',$where);
		foreach($data->result() as $n=>$v){
			//$data->row($n)->foto = base_url();
		}
		return $data;
	}

	function remplazar($formula){
		$this->db->order_by('id','DESC');
		foreach($this->formulas()->result() as $f){
			$formula = str_replace('F'.$f->id,$f->formula,$formula);
		}		
		return $formula;
	}

	function getTarifa($plan,$datos,$printFormula = false){
		$formula = $plan->formula;
		$datos->edad = getAge($datos->fecha_nacimiento);
		$this->db->order_by('desde','DESC');
		$tarifaEdad = $this->tarifas_edades(array('desde <='=>$datos->edad));
		if($tarifaEdad->num_rows()==0){
			$this->db->order_by('desde','DESC');
			$tarifaEdad = $this->tarifas_edades();
		}		
		$TE = $tarifaEdad->row()->importe;
		$SA = $datos->salario_base_diario;
		$DL = $datos->cant_dias_trabajados;	
		$DM = empty($datos->mes)?date("m"):$datos->mes;
		$DM = cal_days_in_month(CAL_GREGORIAN,$DM,date("Y"));
		$this->db->order_by('id','DESC');	
		foreach($this->formulas()->result() as $f){
			$formula = str_replace('F'.$f->id,$f->formula,$formula);
			$formula = $this->remplazar($formula);			
		}
		$formula = str_replace('TE',$TE,$formula);
		$formula = str_replace('SA',$SA,$formula);
		$formula = str_replace('DL',$DL,$formula);
		$formula = str_replace('DM',$DM,$formula);

		if($printFormula){
			echo $formula.'<br/>';
		}
                
                                   try{
                                    eval('$formula = '.$formula.';');
                                   }catch(ParseError $e){
                                       $formula = 0;
                                   }
		return round($formula,2);
	}

	function getTarifaWithFormula($formula,$datos,$printFormula = false){		
		$this->db->order_by('desde','DESC');
		$tarifaEdad = $this->tarifas_edades(array('desde <='=>$datos->edad));
		if($tarifaEdad->num_rows()==0){
			$this->db->order_by('desde','DESC');
			$tarifaEdad = $this->tarifas_edades();
		}		
		$TE = $tarifaEdad->row()->importe;
		$SA = $datos->salario_base_diario;
		$DL = $datos->cant_dias_trabajados;	
		$DM = empty($datos->mes)?date("m"):$datos->mes;
		$DM = cal_days_in_month(CAL_GREGORIAN,$DM,date("Y"));
		$this->db->order_by('id','DESC');	
		foreach($this->formulas()->result() as $f){
			$formula = str_replace('F'.$f->id,$f->formula,$formula);
			$formula = $this->remplazar($formula);			
		}
		$formula = str_replace('TE',$TE,$formula);
		$formula = str_replace('SA',$SA,$formula);
		$formula = str_replace('DL',$DL,$formula);
		$formula = str_replace('DM',$DM,$formula);
		if($printFormula){
			echo $formula.'<br/>';
		}
		eval('$formula = '.$formula.';');
		return round($formula,2);
	}

	function generateCURP($datos){
		$codigoLetra = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','Ñ','O','P','Q','R','S','T','U','V','W','X','Y','Z');        
        /*$datos['apellido'] = 'HERNANDEZ';
        $datos['apellido2'] = 'PADILLA';
        $datos['nombre'] = 'JOSE LUIS';
        $datos['fecha_nacimiento'] = '26/5/1950';
        $datos['sexo'] = 'M';
        $datos['estado_nacimiento'] = 'GUANAJUATO';*/
        $vocales = array('A','E','I','O','U');
        //Comienza tratamiento
        foreach($datos as $n=>$d){
            $datos[$n] = mb_strtoupper($d);
        }
        $datos['apellido'] = str_replace(array('DE ','DE LOS ','LA ','DEL '),array('','','',''),$datos['apellido']);
        $datos['apellido2'] = str_replace(array('DE ','DE LOS ','LA ','DEL '),array('','','',''),$datos['apellido2']);
        $datos['nombre'] = str_replace(array(' DE ',' LOS ',' DEL ','JOSE ','MARIA '),array('','','','',''),$datos['nombre']);
        
        $str = '';
        //Primera letra del apellido
        $str.= substr($datos['apellido'],0,1);
        //Primera vocal del apellido
        if(strlen($datos['apellido'])<2){
            $str.= 'X';
        }else{
            $x = false;
            for($i=1;$i<strlen($datos['apellido']);$i++){
                if(!$x && in_array($datos['apellido'][$i],$vocales)){
                    $str.= $datos['apellido'][$i];
                    $x = true;
                }
            }
        }
        if(empty($datos['apellido2'])){
            $str.= 'X';
        }else{
            $str.= substr($datos['apellido2'],0,1);
        }
        //Primera letra del apellido materno
        $str.= substr($datos['nombre'],0,1);

        //$datos['fecha_nacimiento'] = date("ymd",strtotime(str_replace('/','-',$datos['fecha_nacimiento'])));
        $fecha = new DateTime(str_replace('/','-',$datos['fecha_nacimiento']));
        $datos['fecha_nacimiento'] = $fecha->format('ymd');
        //Fecha nacimiento
        $str.= $datos['fecha_nacimiento'];

        //Sexo
        $str.= $datos['sexo']=='M'?'H':'M';

        $datos['estado_nacimiento'] = mb_strtoupper($datos['estado_nacimiento']);
        $clave = $this->db->get_where('estados',array('nombre'=>$datos['estado_nacimiento']));
        if($clave->num_rows()>0){
            $clave = $clave->row()->clave;
        }else{
            $clave = 'XX';
        }
        //Clave estado
        $str.= $clave;

        //Segunda consonante apellido
        if(strlen($datos['apellido'])<3){
            $str.= 'X';
        }else{
            $x = false;
            for($i=2;$i<strlen($datos['apellido']);$i++){
                if(!$x && !in_array($datos['apellido'][$i],$vocales)){
                    $str.= $datos['apellido'][$i];
                    $x = true;
                }
            }
        }

        //Segunda consonante apellido materno
        if(empty($datos['apellido2']) || strlen($datos['apellido2'])<3){
            $str.= 'X';
        }else{
            $x = false;
            for($i=2;$i<strlen($datos['apellido2']);$i++){
                if(!$x && !in_array($datos['apellido2'][$i],$vocales)){
                    $str.= $datos['apellido2'][$i];
                    $x = true;
                }
            }
        }

        //Segunda consonante del nombre
        if(empty($datos['nombre']) || strlen($datos['nombre'])<2){
            $str.= 'X';
        }else{
            $x = false;
            for($i=1;$i<strlen($datos['nombre']);$i++){
                if(!$x && !in_array($datos['nombre'][$i],$vocales)){
                    $str.= $datos['nombre'][$i];
                    $x = true;
                }
            }
        }

        //Concatenamos un 0
        $str.= '0';

        //Ultimo digito
        $last = array_search(substr($str,0,1),$codigoLetra)*18;
        $last+= array_search(substr($str,1,1),$codigoLetra)*17;
        $last+= array_search(substr($str,2,1),$codigoLetra)*16;
        $last+= array_search(substr($str,3,1),$codigoLetra)*15;
        $last+= array_search(substr($str,4,1),$codigoLetra)*14;
        $last+= array_search(substr($str,5,1),$codigoLetra)*13;
        $last+= array_search(substr($str,6,1),$codigoLetra)*12;
        $last+= array_search(substr($str,7,1),$codigoLetra)*11;
        $last+= array_search(substr($str,8,1),$codigoLetra)*10;
        $last+= array_search(substr($str,9,1),$codigoLetra)*9;
        $last+= array_search(substr($str,10,1),$codigoLetra)*8;
        $last+= array_search(substr($str,11,1),$codigoLetra)*7;
        $last+= array_search(substr($str,12,1),$codigoLetra)*6;
        $last+= array_search(substr($str,13,1),$codigoLetra)*5;
        $last+= array_search(substr($str,14,1),$codigoLetra)*4;
        $last+= array_search(substr($str,15,1),$codigoLetra)*3;
        $last+= array_search(substr($str,16,1),$codigoLetra)*2;
        $last = $last%10-10;
        $last = $last<=-10?0:$last*-1;
        $str.= $last;
        return $str;
	}
	
}