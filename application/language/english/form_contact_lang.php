<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['success_response_contact_form'] = 'Thank you for contact us, shortly we will contact you';
$lang['error_response_contact_form'] = 'Please, complete the requested information  <script>$("#guardar").attr("disabled",false); </script>';
$lang['error_response_captcha_form'] = 'The captcha field is incorrect';

$lang['asunto2'] = 'request contact to you';
$lang['titulo'] = 'CONTACT FORM FROM WEB';
$lang['Nom'] = 'Name';
$lang['asunto'] = 'Subject';
$lang['telefono'] = 'Phone';
$lang['message'] = 'Message';

$lang['subscribe'] = 'Thank for register';
$lang['subscribeText'] = 'Hi {email}, Thank for register to our newsletter list';

$lang['subscribeBaja'] = 'Si deseas darte de baja pulsa en el siguiente <a href="'.base_url().'paginas/frontend/unsubscribe/{email}">enlace</a>';
$lang['subscribeBajaTitulo'] = 'Usuario quiere darse de baja de los boletines';
$lang['subscribeBajaText'] = 'Hola, te contactamos desde la web para informarte que el usuario {email} no desea recibir mas boletines desde nuestra web.';
$lang['subscribeBajaSuccess'] = 'Dado de baja correctamente';


$lang['contacto_email_subs_error'] = 'Este correo ya está registrado';
$lang['contactO_email_subs_success'] = 'La subscripción se ha realizado con éxito';

$lang['medicacio_success'] = 'Solicitud enviada. En breve nos pondremos en contacto.';

$lang['solicitud_medicacio'] = 'Solicitud de fórmula';
$lang['solicitud_asunto2'] = 'Solicita una fórmula';


$lang['solicitud_formula_nombre'] =  'Nombre';
$lang['solicitud_formula_telefono'] =  'Teléfono';
$lang['solicitud_formula_email'] =  'Email';
$lang['solicitud_formula_cp'] =  'C.P.';
$lang['solicitud_formula_direccion'] =  'Dirección';
$lang['solicitud_formula_ciudad'] =  'Ciudad';
$lang['solicitud_formula_provincia'] =  'Província';
$lang['solicitud_formula_receta1'] = 'Receta1';
$lang['solicitud_formula_receta2'] = 'Receta2';
$lang['solicitud_formula_receta3'] = 'Receta3';
$lang['solicitud_formula_comentarios'] = 'Comentarios';

$lang['solicitud_gracias'] = ' 
<p>gracias por tu solicitud de fórmula. En breve nos pondremos en contacto para concretar la recogida. </p>
<p>A continuación el detalle de tu solicitud:</p>';