var croppers = [];
var crops = [];
var example = '';

function imageSlimUpload(error, data, response){
	var inp = $("#field-"+data.meta.name);
	var x = '';
	$(inp).parents('.form-group').find('input[name="slim[]"]').each(function(){
		var js = JSON.parse($(this).val());
		x+= js.file+',';
		inp.val(x);
	});
}

$(document).on('ready',function(){
	$(".slimGallery").each(function(){		
		initCrop($(this));
		var val = $(this).find('input[name="slim[]"]').val();
		$(this).find('input[name="slim[]"]').val(JSON.stringify({file:val}));
	});	

	$(".addCrop").on('click',function(e){
    	addCrop(e,this);
    });

    $(document).on('click',".image-erase",function(e){
    	e.preventDefault();
        var contenedor = $(this).parents('.form-group').find('.parentCrop');
    	var row = $(this).parents('.slimGallery');
    	row.remove();
    	var inp = contenedor.find('.fieldValue');        	
		var x = '';
		$(inp).parents('.form-group').find('input[name="slim[]"]').each(function(){
			console.log($(this).val());
			var js = JSON.parse($(this).val());
			x+= js.file+',';
			inp.val(x);
		});
	    });
});

function addCrop(e,el){
	e.preventDefault();
	var newel = example.clone();	
	newel.attr('id',newel.attr('id')+$(document).find('.slimGallery').length);
	newel.find('img').remove();
	$(el).parents('.form-group').find('.parentCrop').append(newel);	
	initCrop(newel);	
}

function initCrop(el){	
	example = example==''?el.clone():example;	
	var c = $(el).slim();
    crops.push(c);    
}