function imageSlimUpload(error, data, response) {
    if(error!=null){
    	return false;
    }
    $("#field-"+response.field).val(response.file);
    $("#field-"+response.field).attr('data-val',response.file);
}

function addValueSlim(data,ready){	
	var div = $("#field-"+data.meta.name).parents('.slim');
	data.meta.before = div.find('img').attr('src');
	ready(data);
}

function closeModalManager(){
	window.onSelModalCropPhoto = undefined;
	$("#modalFileManager").modal('toggle');
}

function openFileManager(url,el){

	window.onSelModalCropPhoto = function(file){				
		var p = $('#'+el).parent();
		$('#'+el).find('img').remove();
		$('#'+el).find('input[type="hidden"]').val('');
		var c = $('#'+el).clone();					
		var cropper = new Slim(document.getElementById(el),{
			push:true,
			service: $('#'+el).data('service'),		 
		     post:$('#'+el).data('post'),
		     size:$('#'+el).data('size'),
		     instantEdit:$('#'+el).data('instant-edit'),
		     push:$('#'+el).data('push'),
		     didUpload:imageSlimUpload,
		     download:$('#'+el).data('download'),
		     willSave:addValueSlim,
		     label:$('#'+el).data('label'),
		     metaName:$('#'+el).data('meta-name'),
		     forceSize:$('#'+el).data('force-size'),
		});
		cropper.load(url+file);
		closeModalManager();
	}
	if($("#modalFileManager").length==0){
		var modal = '<div class="modal fade" id="modalFileManager" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">\
			  <div class="modal-dialog modal-lg" role="document">\
			    <div class="modal-content">\
			      <div class="modal-header">\
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
			        <h4 class="modal-title" id="myModalLabel">Modal title</h4>\
			      </div>\
			      <div class="modal-body">\
			        <iframe src="'+url+'assets/grocery_crud/texteditor/tiny_mce/plugins/filemanager/dialog.php?editor=foto" style="border:0; width:100%; height:500px;"></iframe>\
			      </div>\
			      <div class="modal-footer">\
			        <button type="button" class="btn btn-default mce-close" onclick="closeModalManager()">Cerrar</button>\
			      </div>\
			    </div>\
			  </div>\
			</div>';
		$("body").append(modal);			
		$("body").on('shown.bs.modal','#modalFilemanager',function(){console.log('asd'); window.onSelModalCropPhoto = undefined;});	
	}
	$("#modalFileManager").modal('toggle');	
}